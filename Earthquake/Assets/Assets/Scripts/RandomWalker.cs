﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RandomWalker : MonoBehaviour
{
    float MinimumRange;
    public float MaximumRange;
    NavMeshAgent navMeshAgent;
    // Vector3 randomDestination { get { return new Vector3(Random.Range(MinimumRange, MaximumRange), navMeshAgent.height / 2, Random.Range(MinimumRange, MaximumRange)); } }
    // Use this for initialization
    void Start()
    {
        MinimumRange = MaximumRange * -1;
        navMeshAgent = GetComponent<NavMeshAgent>();
        navMeshAgent.SetDestination(randomDestination());
        Debug.Log(navMeshAgent.destination);
    }

    // Update is called once per frame
    void Update()
    {
        if (!navMeshAgent.hasPath)
        {
            navMeshAgent.SetDestination(randomDestination());
        }
        if (navMeshAgent.path.status.Equals(NavMeshPathStatus.PathInvalid))
            navMeshAgent.SetDestination(randomDestination());
    }

    Vector3 randomDestination()
    {
        return new Vector3(transform.position.x + Random.Range(MinimumRange, MaximumRange), navMeshAgent.height / 2, transform.position.z + Random.Range(MinimumRange, MaximumRange));
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, MaximumRange);
    }
}
