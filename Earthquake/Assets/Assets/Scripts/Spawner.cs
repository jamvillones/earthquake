﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject PersonPrefab;
    public Transform SpawnPoint;
    public float SpawnRate;
    public float MaximumNumberofPersons;
    List<GameObject> spawnedPersons = new List<GameObject>();

    // Use this for initialization
    void Start()
    {
        StartCoroutine(spawnRoutine());
    }


    IEnumerator spawnRoutine()
    {
        GameObject temp = Instantiate(PersonPrefab, SpawnPoint.position, Quaternion.identity);
        spawnedPersons.Add(temp);
        yield return new WaitForSecondsRealtime(SpawnRate);
        if (spawnedPersons.Count < MaximumNumberofPersons)
            StartCoroutine(spawnRoutine());
    }
}
